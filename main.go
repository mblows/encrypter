package main

import (
	"crypto/subtle"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
)

func main() {
	iterations := flag.Int("i", 1, "Number of iterations used for the Argon2 key derivation function")
	memory := flag.Int("m", 64, "Memory used for the Argon2 key derivation function in MB")
	parallelism := flag.Int("p", runtime.NumCPU(), "Parallelism used for the Argon2 key derivation function")
	encrypt := flag.Bool("e", false, "Encrypt the input file")
	decrypt := flag.Bool("d", false, "Decrypt the input file")
	flag.Parse()

	if len(flag.Args()) != 2 {
		fmt.Printf("Usage: %s [options] <input file> <output file>\n", os.Args[0])
		return
	}

	if *encrypt && *decrypt {
		fmt.Println("Cannot encrypt and decrypt at the same time")
		return
	}

	if *iterations < 1 {
		fmt.Println("Number of iterations must be greater than 0")
		return
	}

	if *memory < 1 {
		fmt.Println("Memory must be greater than 0")
		return
	}

	if *parallelism < 1 {
		fmt.Println("Parallelism must be greater than 0")
		return
	}

	if !(*encrypt || *decrypt) {
		*encrypt = true
	}

	inputFile := flag.Args()[0]
	outputFile := flag.Args()[1]

	if !isFile(inputFile) {
		fmt.Printf("Input file %s does not exist, or isn't a regular file\n", inputFile)
		return
	}

	for isDir(outputFile) {
		outputFile = filepath.Join(outputFile, filepath.Base(outputFile))
	}

	if *encrypt {
		password := getPassword("Enter password: ")
		if password == nil {
			fmt.Println("Error reading password")
			return
		}
		defer zeroBytes(password)

		repeat := getPassword("Repeat password: ")
		if repeat == nil {
			fmt.Println("Error reading repeated password")
			return
		}
		defer zeroBytes(repeat)

		if subtle.ConstantTimeCompare(password, repeat) != 1 {
			fmt.Println("Passwords do not match")
			return
		}

		if err := encryptFile(inputFile, outputFile, password, uint32(*iterations), uint32(*memory), uint8(*parallelism)); err != nil {
			fmt.Printf("Error encrypting file: %s\n", err)
			return
		}
	} else {
		password := getPassword("Enter password: ")
		if password == nil {
			fmt.Println("Error reading password")
			return
		}

		if err := decryptFile(inputFile, outputFile, password); err != nil {
			fmt.Printf("Error decrypting file: %s\n", err)
			return
		}
	}
}
