package main

import (
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"os"

	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/chacha20poly1305"
)

const (
	nonceSize = 24 // Size of the nonce used for XChaCha20-Poly1305
	keySize   = 32 // Size of the key used for XChaCha20-Poly1305
	saltSize  = 16 // Size of the salt used for the Argon2 key derivation function
)

type Header struct {
	Iterations          uint32 // Number of iterations used for the key derivation function. 4 bytes.
	Memory              uint32 // Memory used for the key derivation function in MB. 4 bytes.
	Parallelism         uint8  // Parallelism used for the key derivation function. 1 byte.
	Salt                []byte // Salt used for the key derivation function.
	KeyEncryptionNonce  []byte // 24 byte nonce used for the key encryption.
	DataEncryptionNonce []byte // 24 byte nonce used for the data encryption.
	EncryptedDataKey    []byte // 48 byte encrypted data encryption key. 32 byte key + 16 byte tag
}

// generateRandomBytes returns a slice of random bytes of length n.
func generateRandomBytes(size int) ([]byte, error) {
	r := make([]byte, size)
	if _, err := rand.Read(r); err != nil {
		return nil, err
	}

	return r, nil
}

// createHeader creates a header from the salt, encrypted key, nonce and iterations.
func createHeader(salt, keyEncryptionNonce, encryptedDataKey, dataEncryptionNonce []byte, iterations, memory uint32, parallelism uint8) []byte {
	/*
		4 byte iterations (uint32)
		4 byte memory (uint32)
		1 byte parallelism (uint8)
		16 byte salt used for the key derivation function
		24 byte nonce used for the key encryption
		24 byte nonce used for the data encryption
		48 byte encrypted data encryption key: 32 byte key + 16 byte tag
	*/

	header := make([]byte, 0, 4+4+1+16+24+24+48)
	header = append(header, putInt(iterations)...)
	header = append(header, putInt(memory)...)
	header = append(header, putInt(parallelism)...)
	header = append(header, salt...)
	header = append(header, keyEncryptionNonce...)
	header = append(header, dataEncryptionNonce...)
	header = append(header, encryptedDataKey...)
	return header
}

// parseHeader reads a header from a byte slice.
func parseHeader(headerSlice []byte) (header Header, err error) {
	/*
		4 byte iterations (uint32)
		4 byte memory (uint32)
		1 byte parallelism (uint8)
		16 byte salt used for the key derivation function
		24 byte nonce used for the key encryption
		24 byte nonce used for the data encryption
		48 byte encrypted data encryption key: 32 byte key + 16 byte tag
	*/

	if len(headerSlice) != 121 {
		return header, fmt.Errorf("invalid header length, expected 121 bytes, got %d", len(headerSlice))
	}

	header.Iterations = binary.BigEndian.Uint32(headerSlice[:4])
	header.Memory = binary.BigEndian.Uint32(headerSlice[4:8])
	header.Parallelism = headerSlice[8]
	header.Salt = headerSlice[9:25]
	header.KeyEncryptionNonce = headerSlice[25:49]
	header.DataEncryptionNonce = headerSlice[49:73]
	header.EncryptedDataKey = headerSlice[73:121]
	return header, nil
}

// encrypt encrypts a plaintext with a key and nonce, and returns the ciphertext.
// If successful, the ciphertext is written to the plaintext slice.
func encrypt(data, key, nonce []byte) (ciphertext []byte, err error) {
	aead, err := chacha20poly1305.NewX(key)
	if err != nil {
		return nil, err
	}

	// Encrypt the data, using the plaintext slice as the output buffer
	ciphertext = aead.Seal(data[:0], nonce, data, nil)
	return ciphertext, nil
}

// decrypt decrypts a ciphertext with a key and nonce, and returns the plaintext.
// If successful, the plaintext is written to the ciphertext slice.
func decrypt(data, key, nonce []byte) (plaintext []byte, err error) {
	aead, err := chacha20poly1305.NewX(key)
	if err != nil {
		return nil, err
	}

	// Decrypt the data, using the ciphertext slice as the output buffer
	plainText, err := aead.Open(data[:0], nonce, data, nil)
	if err != nil {
		return nil, err
	}

	return plainText, nil
}

// encryptFile encrypts a file with a password.
func encryptFile(src, dst string, password []byte, iterations, memory uint32, parallelism uint8) error {
	plaintext, err := os.ReadFile(src)
	if err != nil {
		return fmt.Errorf("error reading file: %s", err)
	}

	// Generate a random key
	dataEncryptionKey, err := generateRandomBytes(keySize)
	if err != nil {
		return fmt.Errorf("error generating random key: %s", err)
	}

	defer zeroBytes(dataEncryptionKey)

	// Generate a random nonce to encrypt the data
	dataEncryptionNonce, err := generateRandomBytes(nonceSize)
	if err != nil {
		return fmt.Errorf("error generating random nonce: %s", err)
	}

	// Generate a random nonce to encrypt the key
	keyEncryptionNonce, err := generateRandomBytes(nonceSize)
	if err != nil {
		return fmt.Errorf("error generating random nonce: %s", err)
	}

	// Generate a random salt for the key derivation function
	salt, err := generateRandomBytes(saltSize)
	if err != nil {
		return fmt.Errorf("error generating random salt: %s", err)
	}

	// Derive the key encryption key from the password.
	keyEncryptionKey := argon2.IDKey(password, salt, iterations, memory, parallelism, keySize)
	defer zeroBytes(keyEncryptionKey)

	// Encrypt the data encryption key with the key encryption key
	encryptedKey, err := encrypt(dataEncryptionKey, keyEncryptionKey, keyEncryptionNonce)
	if err != nil {
		return fmt.Errorf("error encrypting key: %s", err)
	}

	// Create the output file
	outFile, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("error creating file: %s", err)
	}
	defer outFile.Close()

	// Write the salt, encrypted key and nonce, and iterations to the header
	header := createHeader(salt, keyEncryptionNonce, encryptedKey, dataEncryptionNonce, iterations, memory, parallelism)

	// Encrypt the file
	ciphertext, err := encrypt(plaintext, dataEncryptionKey, dataEncryptionNonce)
	if err != nil {
		_ = os.Remove(dst)
		return fmt.Errorf("error encrypting file: %s", err)
	}

	// Get the total header length, and write it to the file
	headerLen := len(header)
	headerLenBytes := make([]byte, 4)
	binary.BigEndian.PutUint32(headerLenBytes, uint32(headerLen))
	if _, err := outFile.Write(headerLenBytes); err != nil {
		return fmt.Errorf("error writing header length: %s", err)
	}

	// Write the header to the file
	if _, err := outFile.Write(header); err != nil {
		return fmt.Errorf("error writing header: %s", err)
	}

	// Write the encrypted data to the file
	if _, err := outFile.Write(ciphertext); err != nil {
		return fmt.Errorf("error writing ciphertext: %s", err)
	}

	return nil
}

// decryptFile decrypts a file with a password.
func decryptFile(src, dst string, password []byte) error {
	// Read the file
	data, err := os.ReadFile(src)
	if err != nil {
		return fmt.Errorf("failed to read file: %v", err)
	}

	// Get the header length
	headerLen := binary.BigEndian.Uint32(data[:4])
	if len(data) < int(4+headerLen) {
		return fmt.Errorf("invalid file: data too short for header, expected at least %d bytes but got %d", 4+headerLen, len(data))
	}
	ciphertext := data[4+headerLen:]

	// Read the header
	header, err := parseHeader(data[4 : 4+headerLen])
	if err != nil {
		return fmt.Errorf("failed to read header: %v", err)
	}

	// Derive the key encryption key from the password.
	keyEncryptionKey := argon2.IDKey(password, header.Salt, header.Iterations, header.Memory, header.Parallelism, keySize)
	defer zeroBytes(keyEncryptionKey)

	// Decrypt the data encryption key with the key encryption key
	dataEncryptionKey, err := decrypt(header.EncryptedDataKey, keyEncryptionKey, header.KeyEncryptionNonce)
	if err != nil {
		return fmt.Errorf("failed to decrypt key: %v", err)
	}
	defer zeroBytes(dataEncryptionKey)

	// Decrypt the file
	plainText, err := decrypt(ciphertext, dataEncryptionKey, header.DataEncryptionNonce)
	if err != nil {
		return fmt.Errorf("failed to decrypt data: %v", err)
	}

	// Write the decrypted data
	outFile, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("failed to create output file: %v", err)
	}
	defer outFile.Close()

	if _, err := outFile.Write(plainText); err != nil {
		os.Remove(dst)
		return fmt.Errorf("failed to write decrypted data: %v", err)
	}

	return nil
}
