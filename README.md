# Go File Encrypter

This Go program provides a simple command-line interface for encrypting and decrypting files using the XChaCha20-Poly1305 cipher and the Argon2 key derivation function.

## Usage

```
Usage: program [options] <input file> <output file>

Options:
  -i int
        Number of iterations used for the Argon2 key derivation function (default 1)
  -m int
        Memory used for the Argon2 key derivation function in MB (default 64)
  -p int
        Parallelism used for the Argon2 key derivation function (default number of CPUs)
  -e    Encrypt the input file
  -d    Decrypt the input file
```

If neither `-e` nor `-d` is specified, the program defaults to encryption mode.

## Examples

Encrypt a file:

```shell
encrypter -e secret.txt secret.encrypted
```

Decrypt a file:

```shell
encrypter -d secret.encrypted secret.decrypted
```

Encrypt a file with custom Argon2 parameters:

```shell
encrypter -e -i 10 -m 128 -p 4 secret.txt secret.encrypted
```

## Encryption

The `encryptFile` function takes the following steps:

1. Generates a random 32-byte key for encrypting the data (data encryption key).
2. Generates two random 24-byte nonces: one for encrypting the data encryption key (key encryption nonce), and one for encrypting the data itself (data encryption nonce).
3. Generates a random 16-byte salt for the Argon2 key derivation function.
4. Derives a 32-byte key encryption key from the user-provided password using Argon2.
5. Encrypts the data encryption key with the key encryption key and nonce using XChaCha20-Poly1305.
6. Creates a header containing the Argon2 parameters, salt, nonces, and encrypted data encryption key.
7. Encrypts the input file with the data encryption key and nonce using XChaCha20-Poly1305.
8. Writes the header length, header, and encrypted data to the output file.

## Decryption

The `decryptFile` function takes the following steps:

1. Reads the encrypted file and extracts the header length, header, and encrypted data.
2. Parses the header to obtain the Argon2 parameters, salt, nonces, and encrypted data encryption key.
3. Derives the key encryption key from the user-provided password using Argon2 with the parameters and salt from the header.
4. Decrypts the data encryption key with the key encryption key and nonce using XChaCha20-Poly1305.
5. Decrypts the encrypted data with the data encryption key and nonce using XChaCha20-Poly1305.
6. Writes the decrypted data to the output file.

The program uses the XChaCha20-Poly1305 cipher for encryption and authentication, which provides strong security and is resistant to nonce misuse. The Argon2 key derivation function is used to derive a secure key from the user-provided password, with adjustable memory, parallelism, and iteration parameters to control the cost of key derivation.
