package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"

	"golang.org/x/term"
)

type Numeric interface {
	int8 | int16 | int32 | int64 | uint | uint8 | uint16 | uint32 | uint64
}

// isFile returns true if the path exists and is a regular file.
func isFile(path string) bool {
	if info, err := os.Stat(path); os.IsNotExist(err) {
		return false
	} else {
		return info.Mode().IsRegular()
	}
}

// isDir returns true if the path exists and is a directory.
func isDir(path string) bool {
	if info, err := os.Stat(path); os.IsNotExist(err) {
		return false
	} else {
		return info.Mode().IsDir()
	}
}

// putInt converts a numeric type to a byte slice.
func putInt[N Numeric](n N) []byte {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.BigEndian, n)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

// getPassword reads a password from stdin without echoing it.
func getPassword(prompt string) []byte {
	if prompt != "" {
		fmt.Println(prompt)
	}

	password, err := term.ReadPassword(int(os.Stdin.Fd()))
	if err != nil {
		return nil
	}
	fmt.Println()

	return password
}

// zeroBytes zeroes a byte slice.
func zeroBytes(b []byte) {
	for i := range b {
		b[i] = 0
	}
}
